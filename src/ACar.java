/**
 * Common workflow in java:
 * 
 * Create an interface (Lets us program in general)
 * Create an abstract class (Lets us code shared functionality)
 * Create concrete classes
 * 
 * Abstract:
 * - Cannot be instantiated
 * - Concrete classes that inherit from the abstract class must implement all the abstract methods
 * - Abstract subclasses may or may not implement the abstract methods.
 * 
 * Everything extends object
 * 
 * 
 * @author mkwasnieski
 *
 */
public abstract class ACar implements ICar, java.io.Serializable

{
	
	private String make;
	
	private String model;
	
	private int year;
	
	private int mileage;
	
	public ACar(String inMake, String inModel, int inYear) {
		make = inMake;
		model = inModel;
		year = inYear;
	}
	
	protected void setMileage(int inMileage)
	{
		mileage = inMileage;
	}
	
	@Override
	public String getMake()
	{
		return make;
	}
	
	@Override
	public String getModel() {
		return model;
	}
	
	@Override
	public int getMileage() {
		return mileage;
	}
	
	@Override
	public int getYear() {
		return year;
	}

	@Override
	public String toString(){
		return getMake() + " " + getModel();
	}
	
	
}
